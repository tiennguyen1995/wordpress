<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'secret' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '47uOD/@/-CKtel%+ptxT3%62{IrcJCXJPC`K0CNrm;Cp:m0lHxivre4Odo/uibDl' );
define( 'SECURE_AUTH_KEY',  'DIFj0^g3GFvFptW0KsL@PmQl$bv$]=L=8rx)Df`nq@VfSBUF~JWz_G1LA&%=mMe=' );
define( 'LOGGED_IN_KEY',    '$1<x8uLa_Lt,9mnG?WrzgK9GPsT_dCC=NBdWn&4citB~T}WW7!&E34s]8suXZ`zc' );
define( 'NONCE_KEY',        '2R+&0F~80*4&7Q#^*X/>8A&c{{{FdnCP1K)B1o&B^m*f)$3c0tv}/=Hc4:( tY[j' );
define( 'AUTH_SALT',        '7HXqAKmdYTI[9nP7`]q-yo `m*]$p.%bYAHb3ac=L7pSK3@>K_9.?E0 LU0m(~vD' );
define( 'SECURE_AUTH_SALT', '@)(xh>>*p9(!b1+n?3vzk3ucRL+sI4^{p< =_{JG47 sv}^VBT@h]^U}e>g5a&2?' );
define( 'LOGGED_IN_SALT',   ';NKr2`t5!:!i;K,gb2DmwvaFm]bH?/`SY|XMV2BdV|X7tUv,/zq;A(Vj/`f8o.Uw' );
define( 'NONCE_SALT',       'Q]dy%q/xZbBgqhXr@|M0cK:ks a.6a(=O-U@@.UX4=MCi_uA ]utoG4fL?T!PJy[' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
